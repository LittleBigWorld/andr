package com.example.lab2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.util.HashMap;

public class CreateActivity extends AppCompatActivity {

    private HashMap<String,String> todos;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);
        todos=(HashMap<String,String>) getIntent().getExtras().getSerializable("todos");
        DatePicker picker=findViewById(R.id.createDateField);
        picker.init(2020, 1, 1, new DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

            }
        });
    }
    public void onClickCreate(View view)
    {
        DatePicker date=findViewById(R.id.createDateField);
        EditText text=findViewById(R.id.createTextField);
        if (text.getText().toString().equals(""))
        {
            Toast toast= Toast.makeText(this,"Please fill in add fields",Toast.LENGTH_LONG);
            toast.show();
        }
        else
        {
            String dateString=date.getDayOfMonth()+"."+date.getMonth()+". "+date.getYear();
            String textString=text.getText().toString();
            todos.put(dateString,textString);
            Intent intent=new Intent(this,MainActivity.class);
            intent.putExtra("todos",todos);
            startActivity(intent);
        }
    }
}