package com.example.lab2;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.HashMap;
import java.util.StringTokenizer;

public class DeleteActivity extends AppCompatActivity {

    private HashMap<String, String> todos;
    final int[] count = {0};

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete);
        todos=(HashMap<String,String>) getIntent().getSerializableExtra("todos");
        if (todos == null) todos = new HashMap<>();
        TextView[] views = new TextView[5];
        views[0] = findViewById(R.id.deleteView1);
        views[1] = findViewById(R.id.deleteView2);
        views[2] = findViewById(R.id.deleteView3);
        views[3] = findViewById(R.id.deleteView4);
        views[4] = findViewById(R.id.deleteView5);
        Button[] buttons=new Button[5];
        buttons[0]=findViewById(R.id.btnDelete1);
        buttons[1]=findViewById(R.id.btnDelete2);
        buttons[2]=findViewById(R.id.btnDelete3);
        buttons[3]=findViewById(R.id.btnDelete4);
        buttons[4]=findViewById(R.id.btnDelete5);
        todos.forEach((date, title) ->
        {
            views[count[0]].setText(date+" "+title);
            buttons[count[0]].setVisibility(View.VISIBLE);
            count[0]++;
        });
    }
    public void onClickDelete1(View view)
    {
        TextView textView=findViewById(R.id.deleteView1);
        StringTokenizer tokenizer=new StringTokenizer(textView.getText().toString()," ");
        todos.remove(tokenizer.nextToken());
        Intent intent=new Intent(this,MainActivity.class);
        intent.putExtra("todos",todos);
        startActivity(intent);
    }
    public void onClickDelete2(View view)
    {
        TextView textView=findViewById(R.id.deleteView2);
        StringTokenizer tokenizer=new StringTokenizer(textView.getText().toString()," ");
        todos.remove(tokenizer.nextToken());
        Intent intent=new Intent(this,MainActivity.class);
        intent.putExtra("todos",todos);
        startActivity(intent);
    }
    public void onClickDelete3(View view)
    {
        TextView textView=findViewById(R.id.deleteView3);
        StringTokenizer tokenizer=new StringTokenizer(textView.getText().toString()," ");
        todos.remove(tokenizer.nextToken());
        Intent intent=new Intent(this,MainActivity.class);
        intent.putExtra("todos",todos);
        startActivity(intent);
    }
    public void onClickDelete4(View view)
    {
        TextView textView=findViewById(R.id.deleteView4);
        StringTokenizer tokenizer=new StringTokenizer(textView.getText().toString()," ");
        todos.remove(tokenizer.nextToken());
        Intent intent=new Intent(this,MainActivity.class);
        intent.putExtra("todos",todos);
        startActivity(intent);
    }
    public void onClickDelete5(View view)
    {
        TextView textView=findViewById(R.id.deleteView5);
        StringTokenizer tokenizer=new StringTokenizer(textView.getText().toString()," ");
        todos.remove(tokenizer.nextToken());
        Intent intent=new Intent(this,MainActivity.class);
        intent.putExtra("todos",todos);
        startActivity(intent);
    }
}