package com.example.lab2;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    private HashMap<String,String> todos;
    final int[] count = {0};
    final int MAX_TODOS=5;
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        todos=(HashMap<String,String>) getIntent().getSerializableExtra("todos");
        if (todos==null) todos=new HashMap<>();
        else
        {
            TextView[][] views=new TextView[2][5];
            views[0][0]=findViewById(R.id.dateView1);
            views[0][1]=findViewById(R.id.dateView2);
            views[0][2]=findViewById(R.id.dateView3);
            views[0][3]=findViewById(R.id.dateView4);
            views[0][4]=findViewById(R.id.dateView5);
            views[1][0]=findViewById(R.id.textView1);
            views[1][1]=findViewById(R.id.textView2);
            views[1][2]=findViewById(R.id.textView3);
            views[1][3]=findViewById(R.id.textView4);
            views[1][4]=findViewById(R.id.textView5);
            todos.forEach((date,title)->
            {
                views[0][count[0]].setText(date);
                views[1][count[0]].setText(title);
                count[0]++;
            });
        }
    }
    public void onClickMainAdd(View view)
    {
        if (count[0]>=MAX_TODOS)
        {
            Toast toast= Toast.makeText(this,"Can't add more",Toast.LENGTH_LONG);
            toast.show();
        }
        else
            {
            Intent intent = new Intent(this, CreateActivity.class);
            intent.putExtra("todos", todos);
            startActivity(intent);
        }
    }
    public void onClickMainDelete(View view)
    {
        if (count[0]<=0)
        {
            Toast toast= Toast.makeText(this,"Nothing to delete",Toast.LENGTH_LONG);
            toast.show();
        }
        else
            {
            Intent intent = new Intent(this, DeleteActivity.class);
            intent.putExtra("todos", todos);
            startActivity(intent);
        }
    }
}